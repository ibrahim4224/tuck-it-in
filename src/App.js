import Container from "react-bootstrap/Container"
import "./App.css"
import { useState } from "react"
import "bootstrap/dist/css/bootstrap.min.css"

function App() {
  // Inputted Key
  const [input, setInput] = useState("")

  // Postfix equation
  const [postfix, setPostfix] = useState("Postfix equation here")

  // Last calculation key
  const [lastKey, setLastKey] = useState(null)

  // Error Handling
  const [error, setError] = useState(null)

  // Calculator Buttons
  const btns = [
    "(",
    "CE",
    ")",
    "C",
    "1",
    "2",
    "3",
    "+",
    "4",
    "5",
    "6",
    "-",
    "7",
    "8",
    "9",
    "x",
    ".",
    "0",
    "=",
    "&#xF7;",
  ]

  const checkOperator = (input, text) => {
    console.log(input)
    if (input.length) {
      const lastCharacter = input[input.length - 1]
      let nulled = false
      if (lastCharacter.indexOf("+") > -1) {
        nulled = true
      }
      if (lastCharacter.indexOf("-") > -1) {
        nulled = true
      }
      if (lastCharacter.indexOf("x") > -1) {
        nulled = true
      }
      if (lastCharacter.indexOf("÷") > -1) {
        nulled = true
      }
      if (!nulled) {
        setInput(`${input}${text}`)
      }
    }
  }

  // Handle Button On Click
  const handleBtn = (e) => {
    // Input Text
    const text = e.target.textContent

    // Resetting Error incase
    setError(null)

    // Text check switch
    switch (text) {
      // + Operator key
      case "+":
        checkOperator(input, text)
        break
      // - Operator key
      case "-":
        checkOperator(input, text)
        break
      // x Operator key
      case "x":
        checkOperator(input, text)
        break
      // ÷ Operator key
      case "÷":
        checkOperator(input, text)
        break
      // Clear key
      case "C":
        // Last calculation key reset
        setLastKey(null)
        // Input reset
        setInput("")
        break
      case "CE":
        // Inputt reset
        setInput("")
        break
      case ")":
        let openBracket = false
        input.split("(").forEach((bracket, i) => {
          if (i) {
            if (input.indexOf(")") === -1) {
              openBracket = true
            }
          }
        })
        if (openBracket) {
          setInput(`${input}${text}`)
        }
        break
      case "=":
        if (
          (!error && !isNaN(input[input.length - 1])) ||
          input[input.length - 1] === ")"
        ) {
          // Postfix Equation
          const equation = []
          // Operators in reverse order
          let operators = []

          // Checking each character in input
          input.split("").forEach((val) => {
            // Checking if not error
            // Checking character
            switch (val) {
              // + val Operators
              case "+":
                operators = [val, ...operators]
                equation.push(" ")
                break
              // - val Operators
              case "-":
                operators = [val, ...operators]
                equation.push(" ")
                break
              // x val Operators
              case "x":
                operators = ["*", ...operators]
                equation.push(" ")
                break
              // ÷ val Operators
              case "÷":
                operators = ["/", ...operators]
                equation.push(" ")
                break
              // If close bracket
              case ")":
                // Adding operator inside bracket used
                equation.push(operators[0])
                // Removing operator inside used in bracket
                operators.shift()
                break
              default:
                if (!isNaN(val)) equation.push(val)
            }
          })
          setPostfix(equation.join("") + " " + operators.join(" "))
          let replaceOperator = input.replace("x", "*").replace("÷", "/")
          setLastKey(""+eval(replaceOperator))
          setInput(""+eval(replaceOperator))
        }

        if (error) {
          setInput(error)
        }
        break
      default:
        const ans = `${input}${text}`
        setLastKey(ans)
        setInput(ans)
    }
  }
  return (
    <Container fluid>
      <div className="calculator">
        <input className="input" value={input} readOnly tabIndex="-1" />
        <div className="row g-0 btns">
          {btns.map((key, i) => (
            <div
              key={i}
              role="button"
              tabIndex={i}
              className="col-3"
              dangerouslySetInnerHTML={{ __html: key }}
              onClick={handleBtn}
            />
          ))}
        </div>
        <p className="w-100">{postfix}</p>
      </div>
    </Container>
  )
}

export default App
